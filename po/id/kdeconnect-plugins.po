# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2021, 2022.
# Aziz Adam Adrian <4.adam.adrian@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-17 00:56+0000\n"
"PO-Revision-Date: 2022-08-26 23:44+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: https://t.me/Localizations_KDE_Indonesia\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: battery/batteryplugin.cpp:128
#, kde-format
msgctxt "device name: low battery"
msgid "%1: Low Battery"
msgstr "%1: Baterai Lemah"

#: battery/batteryplugin.cpp:129
#, kde-format
msgid "Battery at %1%"
msgstr "Baterai di %1%"

#. i18n: ectx: property (windowTitle), widget (QWidget, ClipboardConfigUi)
#: clipboard/clipboard_config.ui:17
#, kde-format
msgid "Clipboard plugin"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_1)
#: clipboard/clipboard_config.ui:32
#, kde-format
msgid "Contents shared to other devices"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, check_password)
#: clipboard/clipboard_config.ui:38
#, kde-format
msgid "Passwords (as marked by password managers)"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, check_unknown)
#: clipboard/clipboard_config.ui:45
#, kde-format
msgid "Anything else"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: findthisdevice/findthisdevice_config.ui:17
#, kde-format
msgid "Discovery Utilities"
msgstr "Utilitas Penemuan"

#. i18n: ectx: property (text), widget (QLabel)
#: findthisdevice/findthisdevice_config.ui:25
#, kde-format
msgid "Sound to play:"
msgstr "Suara untuk dibunyikan:"

#. i18n: ectx: property (toolTip), widget (KUrlRequester, soundFileRequester)
#: findthisdevice/findthisdevice_config.ui:39
#, kde-format
msgid "Select the sound to play"
msgstr "Pilih suara yang akan dibunyikan"

#: lockdevice/lockdeviceplugin-win.cpp:65 lockdevice/lockdeviceplugin.cpp:93
#, kde-format
msgid "Remote lock successful"
msgstr "Mengunci jarak jauh berhasil"

#: lockdevice/lockdeviceplugin-win.cpp:68
#: lockdevice/lockdeviceplugin-win.cpp:69 lockdevice/lockdeviceplugin.cpp:98
#: lockdevice/lockdeviceplugin.cpp:100
#, kde-format
msgid "Remote lock failed"
msgstr "Mengunci jarak jauh gagal"

#: notifications/notification.cpp:128
#, kde-format
msgctxt "@action:button"
msgid "Reply"
msgstr "Balas"

#: notifications/notification.cpp:129
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1..."
msgstr "Balas ke %1..."

#: notifications/notification.cpp:135
#, kde-format
msgid "Reply"
msgstr "Balas"

#: notifications/sendreplydialog.cpp:29
#, kde-format
msgid "Send"
msgstr "Kirim"

#. i18n: ectx: property (windowTitle), widget (QDialog, SendReplyDialog)
#: notifications/sendreplydialog.ui:14
#, kde-format
msgid "Dialog"
msgstr "Dialog"

#. i18n: ectx: property (windowTitle), widget (QWidget, PauseMusicConfigUi)
#: pausemusic/pausemusic_config.ui:17
#, kde-format
msgid "Pause music plugin"
msgstr "Plugin jeda musik"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: pausemusic/pausemusic_config.ui:32
#, kde-format
msgid "Condition"
msgstr "Kondisi"

#. i18n: ectx: property (text), widget (QRadioButton, rad_ringing)
#: pausemusic/pausemusic_config.ui:38
#, kde-format
msgid "Pause as soon as phone rings"
msgstr "Jeda segera saat telpon berdering"

#. i18n: ectx: property (text), widget (QRadioButton, rad_talking)
#: pausemusic/pausemusic_config.ui:45
#, kde-format
msgid "Pause only while talking"
msgstr "Jeda hanya ketika berbicara"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: pausemusic/pausemusic_config.ui:61
#, kde-format
msgid "Actions"
msgstr "Aksi"

#. i18n: ectx: property (text), widget (QCheckBox, check_pause)
#: pausemusic/pausemusic_config.ui:67
#, kde-format
msgid "Pause media players"
msgstr "Jeda pemutar media"

#. i18n: ectx: property (text), widget (QCheckBox, check_mute)
#: pausemusic/pausemusic_config.ui:74
#, kde-format
msgid "Mute system sound"
msgstr "Bungkam suara sistem"

#. i18n: ectx: property (text), widget (QCheckBox, check_resume)
#: pausemusic/pausemusic_config.ui:81
#, kde-format
msgid "Automatically resume media when call has finished"
msgstr ""
"Secara otomatis melanjutkan media ketika panggilan telepon sudah selesai"

#: ping/pingplugin.cpp:37
#, kde-format
msgid "Ping!"
msgstr "Ping!"

#: runcommand/runcommand_config.cpp:39
#, kde-format
msgid "Schedule a shutdown"
msgstr "Jadwalkan pematian"

#: runcommand/runcommand_config.cpp:40
#, kde-format
msgid "Shutdown now"
msgstr "Matikan sekarang"

#: runcommand/runcommand_config.cpp:41
#, kde-format
msgid "Cancel last shutdown"
msgstr "Batalkan pematian terakhir"

#: runcommand/runcommand_config.cpp:42
#, kde-format
msgid "Schedule a reboot"
msgstr "Jadwal penyalaan ulang"

#: runcommand/runcommand_config.cpp:43 runcommand/runcommand_config.cpp:52
#, kde-format
msgid "Suspend"
msgstr "Suspensi"

#: runcommand/runcommand_config.cpp:44 runcommand/runcommand_config.cpp:60
#, kde-format
msgid "Lock Screen"
msgstr "Layar Kunci "

#: runcommand/runcommand_config.cpp:47
#, kde-format
msgid "Say Hello"
msgstr "Ucapkan Halo"

#: runcommand/runcommand_config.cpp:50
#, kde-format
msgid "Shutdown"
msgstr "Matikan"

#: runcommand/runcommand_config.cpp:51
#, kde-format
msgid "Reboot"
msgstr "Nyalakan ulang"

#: runcommand/runcommand_config.cpp:55
#, kde-format
msgid "Maximum Brightness"
msgstr "Maximum Kecerahan"

#: runcommand/runcommand_config.cpp:61
#, kde-format
msgid "Unlock Screen"
msgstr "Lepas-kunci Layar"

#: runcommand/runcommand_config.cpp:62
#, kde-format
msgid "Close All Vaults"
msgstr "Tutup Semua Vaults"

#: runcommand/runcommand_config.cpp:64
#, kde-format
msgid "Forcefully Close All Vaults"
msgstr "Tutup Secara Paksa Semua Vaults"

#: runcommand/runcommand_config.cpp:73
#, kde-format
msgid "Sample commands"
msgstr "Contoh perintah"

#: runcommand/runcommand_config.cpp:82
#: sendnotifications/notifyingapplicationmodel.cpp:182
#, kde-format
msgid "Name"
msgstr "Nama"

#: runcommand/runcommand_config.cpp:82
#, kde-format
msgid "Command"
msgstr "Perintah"

#: screensaver-inhibit/screensaverinhibitplugin.cpp:25
#, kde-format
msgid "Phone is connected"
msgstr "Ponsel telah terkoneksi"

#: sendnotifications/notifyingapplicationmodel.cpp:186
#, kde-format
msgid "Blacklisted"
msgstr "Didaftarhitamkan"

#: sendnotifications/notifyingapplicationmodel.cpp:190
#, kde-format
msgid "Name of a notifying application."
msgstr "Nama pada aplikasi yang memberitahu."

#: sendnotifications/notifyingapplicationmodel.cpp:192
#, kde-format
msgid "Synchronize notifications of an application?"
msgstr "Menyinkronkan notifikasi dari sebuah aplikasi?"

#: sendnotifications/notifyingapplicationmodel.cpp:195
#, kde-format
msgid ""
"Regular expression defining which notifications should not be sent.\n"
"This pattern is applied to the summary and, if selected above, the body of "
"notifications."
msgstr ""
"Penentuan ekspresi reguler notifikasi mana yang seharusnya tidak dikirim.\n"
"Pola ini diterapkan pada ringkasan dan jika yang dipilih di atas pada bodi "
"notifikasi."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: sendnotifications/sendnotifications_config.ui:38
#, kde-format
msgid "General"
msgstr "Umum"

#. i18n: ectx: property (toolTip), widget (QCheckBox, check_persistent)
#: sendnotifications/sendnotifications_config.ui:44
#, kde-format
msgid "Synchronize only notifications with a timeout value of 0?"
msgstr "Apakah hanya sinkronkan notifikasi dengan nilai timeout 0?"

#. i18n: ectx: property (text), widget (QCheckBox, check_persistent)
#: sendnotifications/sendnotifications_config.ui:47
#, kde-format
msgid "Persistent notifications only"
msgstr "Notifikasi kukuh saja"

#. i18n: ectx: property (toolTip), widget (QCheckBox, check_body)
#: sendnotifications/sendnotifications_config.ui:60
#, kde-format
msgid ""
"Append the notification body to the summary when synchronizing notifications?"
msgstr ""
"Apakah bubuhkan bodi notifikasi ke ringkasan ketika menyinkronkan notifikasi?"

#. i18n: ectx: property (text), widget (QCheckBox, check_body)
#: sendnotifications/sendnotifications_config.ui:63
#, kde-format
msgid "Include body"
msgstr "Sisipkan bodi"

#. i18n: ectx: property (toolTip), widget (QCheckBox, check_icons)
#: sendnotifications/sendnotifications_config.ui:76
#, kde-format
msgid "Synchronize icons of notifying applications if possible?"
msgstr ""
"Apakah sinkronkan ikon-ikon aplikasi yang memberitahukan jika memungkinkan?"

#. i18n: ectx: property (text), widget (QCheckBox, check_icons)
#: sendnotifications/sendnotifications_config.ui:79
#, kde-format
msgid "Synchronize icons"
msgstr "Sinkronkan ikon-ikon"

#. i18n: ectx: property (toolTip), widget (QSpinBox, spin_urgency)
#: sendnotifications/sendnotifications_config.ui:107
#, kde-format
msgid ""
"<html><head/><body><p>Minimum urgency level of the notifications</p></body></"
"html>"
msgstr ""
"<html><head/><body><p>Minimum level keterdesakan notifikasi</p></body></html>"

#. i18n: ectx: property (toolTip), widget (QLabel, label)
#: sendnotifications/sendnotifications_config.ui:123
#, kde-format
msgid "Synchronize only notifications with the given urgency level."
msgstr "Sinkronkan hanya notifikasi dengan level keterdesakan yang diberikan."

#. i18n: ectx: property (text), widget (QLabel, label)
#: sendnotifications/sendnotifications_config.ui:126
#, kde-format
msgid "Minimum urgency level"
msgstr "Minimum level keterdesakan"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: sendnotifications/sendnotifications_config.ui:148
#, kde-format
msgid "Applications"
msgstr "Aplikasi"

#: sftp/mounter.cpp:168
#, kde-format
msgid "Failed to start sshfs"
msgstr "Gagal menjalankan sshfs"

#: sftp/mounter.cpp:172
#, kde-format
msgid "sshfs process crashed"
msgstr "Proses sshfs mogok"

#: sftp/mounter.cpp:176
#, kde-format
msgid "Unknown error in sshfs"
msgstr "Galat yang tak diketahui dalam sshfs"

#: sftp/mounter.cpp:187
#, kde-format
msgid "Error when accessing filesystem. sshfs finished with exit code %0"
msgstr "Galat ketika mengakses sistem file. sshfs terselesaikan dengan kode %0"

#: sftp/mounter.cpp:196
#, kde-format
msgid "Failed to mount filesystem: device not responding"
msgstr "Gagal mengaitkan sistem file: peranti tidak merespons"

#: sftp/sftpplugin-win.cpp:74
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: sftp/sftpplugin-win.cpp:75
#, kde-format
msgid "Cannot handle SFTP protocol. Apologies for the inconvenience"
msgstr "Tidak bisa menangani protokol SFTP. Maaf atas ketidaknyamanan ini"

#: sftp/sftpplugin.cpp:143
#, kde-format
msgid "All files"
msgstr "Semua file"

#: sftp/sftpplugin.cpp:144
#, kde-format
msgid "Camera pictures"
msgstr "Gambar kamera"

#: share/share_config.cpp:24
#, kde-format
msgid "&percnt;1 in the path will be replaced with the specific device name."
msgstr "&percnt;1 di dalam alur akan digantikan dengan nama peranti tertentu"

#. i18n: ectx: property (windowTitle), widget (QWidget, ShareConfigUi)
#: share/share_config.ui:17
#, kde-format
msgid "Share plugin settings"
msgstr "Pengaturan plugin Share"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: share/share_config.ui:23
#, kde-format
msgid "Receiving"
msgstr "Menerima"

#. i18n: ectx: property (text), widget (QLabel, label)
#: share/share_config.ui:31
#, kde-format
msgid "Save files in:"
msgstr "Simpan file di:"

#: share/shareplugin.cpp:209
#, kde-format
msgid "Could not share file"
msgstr "Tidak bisa berbagi file"

#: share/shareplugin.cpp:209
#, kde-format
msgid "%1 does not exist"
msgstr "%1 tidak ada"

#: telephony/telephonyplugin.cpp:28
#, kde-format
msgid "unknown number"
msgstr "nomor tak diketahui"

#: telephony/telephonyplugin.cpp:37
#, kde-format
msgid "Incoming call from %1"
msgstr "Panggilan masuk dari %1"

#: telephony/telephonyplugin.cpp:41
#, kde-format
msgid "Missed call from %1"
msgstr "Panggilan tidak terjawab dari %1"

#: telephony/telephonyplugin.cpp:69
#, kde-format
msgid "Mute Call"
msgstr "Bungkam Panggilan"

#~ msgid "Use your phone as a touchpad and keyboard"
#~ msgstr "Gunakan telponmu sebagai touchpad dan keyboard"

#~ msgid "Unknown telephony event: %1"
#~ msgstr "Peristiwa telepon tak diketahui: %1"

#~ msgid "SMS from %1<br>%2"
#~ msgstr "SMS dari %1<br>%2"
